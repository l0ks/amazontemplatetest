package com.test.pages.amazon;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;

public class MainPage extends BasePage {
    private Locator searchTextBox = new XPath(".//input[@id='twotabsearchtextbox']");
    private Locator searchDropdownBox = new XPath(".//select[@id='searchDropdownBox']");
    private Locator searchInitBtn = new XPath(".//div[@class='nav-search-submit nav-sprite']//input[@class='nav-input']");

    public void enterText(String inputText){
        waitForElementVisibility(searchTextBox, Locator.Type.ID);
        type("Typing search request: " + inputText,
                inputText,
                searchTextBox,
                Locator.Type.ID);
    }

    public void selectCategory(String categoryName){
        selectDropDownListOptionByText("Choosing category: " + categoryName,
                categoryName,
                searchDropdownBox,
                Locator.Type.ID);
    }

    public void clickSearchBtn(){
        click("Starting search", searchInitBtn, Locator.Type.XPATH);
    }

//    public void initSearchWithCategory(String request, String category){
//        enterText(request);
//        selectCategory(category);
//        click("Starting search", searchInitBtn, Locator.Type.XPATH);
//        //        pressEnter(searchTextBox, Locator.Type.ID);
//    }

}
