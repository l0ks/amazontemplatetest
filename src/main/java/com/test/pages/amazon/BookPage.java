package com.test.pages.amazon;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;
import com.test.models.Book;

public class BookPage extends BasePage {
    private Locator bookTitle = new XPath(".//span[@id='productTitle']");


    public Book getBook(){
        return new Book(
                getElementText("Retrieving book title", bookTitle, Locator.Type.XPATH),
                "sample text",
                "sample text",
                "sample text",
                "sample text"
        );
    }
}
