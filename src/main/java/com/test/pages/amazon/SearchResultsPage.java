package com.test.pages.amazon;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;
import com.test.models.Book;
import com.test.util.reporter.Reporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsPage extends BasePage {
    private Locator searchResults = new XPath(".//div[contains(@class, 's-result-list s-search-results sg-row')]/div[contains(@class,'s-result-item')]");
    private Locator titleLocation = new XPath(".//h2[@class='a-size-mini a-spacing-none a-color-base s-line-clamp-2']");
    private Locator linkLocation = new XPath(".//a[@class='a-link-normal a-text-normal']");

    private Locator authorLocation = new XPath(".//div/span/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/div");
    private Locator authorPartLocation = new XPath(".//span | .//a");


    private Locator ratingLocation = new XPath(".//a[@class='a-popover-trigger a-declarative']//span");

    private Locator priceDivLocation1 = new XPath(".//div[@class='a-section a-spacing-none a-spacing-top-small']");
    private Locator priceDivLocation2 = new XPath(".//div[@class='a-row a-spacing-mini']");
    private Locator pricePaperBackLocation = new XPath("//a[contains(text(),'Paperback')]");

    public List<WebElement> getSearchResults(){
        return isElementPresent(searchResults,Locator.Type.XPATH) ?
                getElements(searchResults, Locator.Type.XPATH) : null;
    }

    public String getBookTitle(WebElement bookDivElement){
        return isElementPresent(bookDivElement, titleLocation, Locator.Type.XPATH) ?
                getElementText("Getting book title", bookDivElement, titleLocation, Locator.Type.XPATH)
                : "Title not available";
    }

    public String getBookAuthor(WebElement bookDivElement){
        StringBuilder authorBuilder = new StringBuilder("");
        List<WebElement> authorElementList =
                bookDivElement.findElement(authorLocation.get(Locator.Type.XPATH))
                        .findElements(authorPartLocation.get(Locator.Type.XPATH));

        for(WebElement element: authorElementList){
            if(element.getAttribute("innerHTML").contains("|")){
                authorBuilder.append(element.getAttribute("innerHTML"));
            }else {
                break;
            }
        }

        return authorBuilder.toString().equals("") ?
                authorBuilder.toString()
                : "Author not available";
    }

    public String getBookRating(WebElement bookDivElement){
        return isElementPresent(bookDivElement, ratingLocation, Locator.Type.XPATH) ?
                getElementText("Getting book rating", bookDivElement, ratingLocation, Locator.Type.XPATH)
                : "Rating not available";
    }

    public String getBookPrice(WebElement bookDivElement, int id){

        StringBuilder builder = new StringBuilder("");
        String price;

        Locator priceTwoOptionLocation = new XPath("//span[@class='celwidget slot=SEARCH_RESULTS template=SEARCH_RESULTS widgetId=search-results index="+ id +"']//a[2]//span[1]");
        Locator priceOneOptionLocation = new XPath("//span[@class='a-price']//span[@class='a-offscreen']");

//        if(bookDivElement.findElements(priceDivLocation1.get(Locator.Type.XPATH)).size() > 0){
//            WebElement priceElement = bookDivElement.findElement(priceDivLocation1.get(Locator.Type.XPATH));
//            if(priceElement.findElements(pricePaperBackLocation.get(Locator.Type.XPATH)).size() > 0){
//                WebElement priceDivBox = priceElement.findElement(pricePaperBackLocation.get(Locator.Type.XPATH));
//                if(priceDivBox.findElements(priceTwoOptionLocation.get(Locator.Type.XPATH)).size() > 0){
//                    builder.append(priceDivBox.findElement(priceTwoOptionLocation.get(Locator.Type.XPATH)).getAttribute("innerHTML"));
//                }
//                else if(priceDivBox.findElements(priceOneOptionLocation.get(Locator.Type.XPATH)).size() > 0){
//                    builder.append(priceDivBox.findElement(priceOneOptionLocation.get(Locator.Type.XPATH)).getAttribute("innerHTML"));
//                }
//
//            }
//        }

        price = !builder.toString().equals("") ? builder.toString() : "Not available";
        return price;
    }

    public String getBookLink(WebElement bookDivElement){
        return isElementPresent(bookDivElement, linkLocation, Locator.Type.XPATH) ?
                getElementAttributeValue("Getting book link", bookDivElement, "href", linkLocation, Locator.Type.XPATH)
                : "Link not available";
    }
}
