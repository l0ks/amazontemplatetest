package com.test.pages.amazon;

import com.test.base.BasePage;
import com.test.locators.Locator;
import com.test.locators.XPath;
import com.test.models.Book;
import com.test.util.reporter.Reporter;
import org.openqa.selenium.WebElement;

import java.util.List;

public class BestsellersPage extends BasePage {
    private Locator bestSellersListLocation = new XPath(".//span/div/span/a/div");

    public List<Book> searchForBestSellers(List<Book> books){
        List<WebElement> bestSellersList = getElements(bestSellersListLocation, Locator.Type.XPATH);

        for (WebElement bestSellerElement : bestSellersList) {
            if(bestSellerElement.getAttribute("Title") != null){
                compareTitle(bestSellerElement.getAttribute("Title"), books);
            }
        }

        return books;
    }

    private void compareTitle(String title, List<Book> books){
        for(Book book: books){
            if(!book.isBestseller()){
                book.checkTitleForBestseller(title);
            }
            else {
                break;
            }
        }
    }

}
