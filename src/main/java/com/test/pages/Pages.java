package com.test.pages;

import com.test.pages.amazon.BestsellersPage;
import com.test.pages.amazon.BookPage;
import com.test.pages.amazon.MainPage;
import com.test.pages.amazon.SearchResultsPage;

public class Pages {

    private static LoginPage loginPage;

    private static MainPage mainPage;

    private static SearchResultsPage searchResultsPage;

    private static BestsellersPage bestsellersPage;

    private static BookPage bookPage;

    public static LoginPage loginPage() {
        if (loginPage == null){
            loginPage = new LoginPage();
        }
        return loginPage;
    }

    public static MainPage mainPage() {
        if (mainPage == null){
            mainPage = new MainPage();
        }
        return mainPage;
    }

    public static SearchResultsPage searchResultsPage() {
        if (searchResultsPage == null){
            searchResultsPage = new SearchResultsPage();
        }
        return searchResultsPage;
    }

    public static BestsellersPage bestsellersPage() {
        if (bestsellersPage == null){
            bestsellersPage = new BestsellersPage();
        }
        return bestsellersPage;
    }

    public static BookPage bookPage() {
        if (bookPage == null){
            bookPage = new BookPage();
        }
        return bookPage;
    }

}
