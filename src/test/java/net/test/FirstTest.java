package net.test;

import com.test.base.BaseTest;
import com.test.locators.Locator;
import com.test.models.Book;
import com.test.pages.Pages;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;


import static com.test.util.Constants.*;

public class FirstTest extends BaseTest {

    @Test
    private void searchForBook(){
        driver.get(MAIN_PAGE_URL);
        Pages.mainPage().enterText(SEARCH);
        Pages.mainPage().selectCategory(CATEGORY);
        Pages.mainPage().clickSearchBtn();

        List<Book> books = getBookList(Pages.searchResultsPage().getSearchResults());

        driver.get(BESTSELLER_URL);
        books = Pages.bestsellersPage().searchForBestSellers(books);

        driver.get(REQUEST_URL);
        Book requestedBook = Pages.bookPage().getBook();

        System.out.println(searchCollection(requestedBook, books) ? "Match found!" : "No match...");
    }

    private List<Book> getBookList(List<WebElement> elementList){
        List<Book> books = new ArrayList<>();
        int x = 0;
        for (WebElement element: elementList) {
            Book book = new Book(
                    Pages.searchResultsPage().getBookTitle(element),
                    Pages.searchResultsPage().getBookAuthor(element),
                    Pages.searchResultsPage().getBookPrice(element, x),
                    Pages.searchResultsPage(). getBookRating(element),
                    Pages.searchResultsPage().getBookLink(element)
            );
            books.add(book);
        }
        return books;
    }

    private boolean searchCollection(Book inputBook, List<Book> books){
        boolean result = false;
        for (Book book: books) {
            if(inputBook.equals(book)){
                result = true;
                break;
            }
        }
        return result;
    }

}
